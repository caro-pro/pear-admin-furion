﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace Pear.EntityFramework.Core
{
    [AppDbContext("Pear", DbProvider.MySql)]
    public class PearDbContext : AppDbContext<PearDbContext>
    {
        public PearDbContext(DbContextOptions<PearDbContext> options) : base(options)
        {
        }
    }
}
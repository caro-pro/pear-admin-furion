﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pear.Core.SeedData
{
    /// <summary>
    /// 用户表种子数据 <see cref="Security"/>
    /// </summary>
    public class SecuritySeedData : IEntitySeedData<Security>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<Security> HasData(DbContext dbContext, Type dbContextLocator)
        {
            var securities = typeof(SecurityConsts).GetFields().Select(u => u.GetRawConstantValue().ToString()).ToArray();
            var list = new List<Security>();
            for (var i = 1; i < securities.Length + 1; i++)
            {
                list.Add(new Security { Id = i, UniqueName = securities[i - 1], CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"), IsDeleted = false });
            }

            return list;
        }
    }
}

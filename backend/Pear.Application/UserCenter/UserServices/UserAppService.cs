﻿using Furion.DatabaseAccessor;
using Furion.DataEncryption;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pear.Application.Enums;
using Pear.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pear.Application.UserCenter
{
    /// <summary>
    /// 用户服务接口
    /// </summary>
    [ApiDescriptionSettings(ApiGroupConsts.USER_CENTER)]
    public class UserAppService : IDynamicApiController, ITransient
    {
        /// <summary>
        /// 非泛型仓储，只需要初始化一个即可
        /// </summary>
        private readonly IRepository _repository;

        /// <summary>
        /// 构造函数
        /// </summary>
        public UserAppService(IRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        /// <param name="input"></param>
        /// <remarks>
        /// 用户名/密码：admin/admin
        /// </remarks>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<LoginOutput> Login([FromServices] IHttpContextAccessor httpContextAccessor, LoginInput input)
        {
            // 获取加密后的密码
            var encryptPassword = MD5Encryption.Encrypt(input.Password.Trim());

            // 判断用户名或密码是否正确
            var user = await _repository.Change<User>()
                                        .FirstOrDefaultAsync(u => u.Account.Equals(input.Account) && u.Password.Equals(encryptPassword));
            _ = user ?? throw Oops.Oh(ServiceErrorCodes.u1000);

            // 更新登录时间
            var signinedTime = DateTimeOffset.Now;
            user.SigninedTime = signinedTime;

            // 映射结果
            var output = user.Adapt<LoginOutput>();

            // 生成 token
            var accessToken = output.AccessToken = JWTEncryption.Encrypt(new Dictionary<string, object>
            {
                { "UserId",user.Id },
                { "Account",user.Account }
            });

            // 生成 刷新token
            var refreshToken = JWTEncryption.GenerateRefreshToken(accessToken);

            // 设置 Swagger 自动登录
            httpContextAccessor.SigninToSwagger(accessToken);

            // 设置刷新 token
            httpContextAccessor.HttpContext.Response.Headers["x-access-token"] = refreshToken;

            return output;
        }
    }
}

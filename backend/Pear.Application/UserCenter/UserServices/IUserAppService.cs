﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Pear.Application.UserCenter
{
    /// <summary>
    /// 用户服务接口
    /// </summary>
    public interface IUserAppService
    {
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        /// <param name="input"></param>
        /// <remarks>
        /// 用户名/密码：admin/admin
        /// </remarks>
        /// <returns></returns>
        Task<LoginOutput> Login([FromServices] IHttpContextAccessor httpContextAccessor, LoginInput input);
    }
}

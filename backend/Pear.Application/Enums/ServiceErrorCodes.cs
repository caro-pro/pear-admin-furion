﻿using Furion.FriendlyException;
using System.ComponentModel;

namespace Pear.Application.Enums
{
    /// <summary>
    /// 业务日志错误码
    /// </summary>
    [ErrorCodeType]
    public enum ServiceErrorCodes
    {
        /// <summary>
        /// 用户名或密码不正确
        /// </summary>
        [Description("用户名或密码不正确"), ErrorCodeItemMetadata("用户名或密码不正确")]
        u1000
    }
}

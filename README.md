# Pear Admin Furion

⚡  基于 .Net 5 平台 Furion 生态的落地实践


building ...  :boom: 

# To do something

- [x] 项目起草
- [x] 登录入口
- [ ] 基础设施
- [ ] 权限处理
- [ ] 用户管理
- [ ] 角色管理
- [ ] 权限管理
- [ ] 资源管理
- [ ] 数据字典
- [ ] 配置中心
- [ ] 系统监控
- [ ] 定时任务
- [ ] 行为日志
 
# Preview image

![](readmes/1.jpg)

![](readmes/2.jpg)

![](readmes/3.jpg)

| 移动  | 预览  |  
|---|---|
| ![](readmes/4.jpg)  |  ![](readmes/5.jpg) |
|---|---|